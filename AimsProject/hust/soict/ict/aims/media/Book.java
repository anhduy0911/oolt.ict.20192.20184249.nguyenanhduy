package hust.soict.ict.aims.media;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Book extends Media implements Comparable 
{
    private String content;
    private List<String> contentTokens = new ArrayList<String>();
    private Map<String, Integer> wordFrequency = new HashMap<String, Integer>(); 

    private List<String> authors = new ArrayList<String>();

    public Book() {
        super();
    }

    public Book(String title) {
        super(title);
    }

    public Book(String title, String category) {
        super(title,category);
    }

    public Book(String title, String category, float cost,List<String> authors)
    {
       super(title,category,cost);
       this.authors = authors;

    }

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public void addAuthor(String name)
    {
        for (String au : authors) {
            if(au.equals(name))
            {
                System.out.println("The author has already been added!");
                return;
            }
        }
        
        authors.add(name);
    }

    public void removeAuthor(String name)
    {
        for (String au : authors) {
            if(au.equals(name))
            {
                authors.remove(name);
                return;
            }
        }
        
        System.out.println("There is no such author");
    }

    public void setContent(String content) {
        this.content = content;
        processContent();
    }

    private void processContent()
    {
        contentTokens = Arrays.asList(content.split("[ ,.]"));
        Collections.sort(contentTokens);
        for (String token : contentTokens) {
            if (wordFrequency.containsKey(token)) {
                wordFrequency.put(token, wordFrequency.get(token) + 1);
            }
            else
                wordFrequency.put(token, 1);
        }
    }

    public int compareTo(Object o)
    {
        Book m = (Book) o; //downcast pose a potential issue.
        return this.getTitle().compareTo(m.getTitle());
    }

    private void printFrequency()
    {
        Set tok = wordFrequency.keySet();
        Iterator iter = tok.iterator();
        while (iter.hasNext()) {
            String token = (String)iter.next();
            System.out.println(token + ": " + wordFrequency.get(token));
        }
    }

    @Override
    public String toString()
    {
        printFrequency();
        return String.format("Content: " + this.content + "\nContent length: " + contentTokens.size());
    }
}
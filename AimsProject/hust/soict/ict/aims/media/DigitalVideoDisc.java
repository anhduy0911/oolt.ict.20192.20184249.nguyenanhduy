package hust.soict.ict.aims.media;

import java.util.List;
import java.util.Arrays;
import hust.soict.ict.aims.PlayerException;

public class DigitalVideoDisc extends Disc implements Playable, Comparable {

    // Different Overloads of Constructors

    public DigitalVideoDisc(String title) {
        super(title);
    }

    public DigitalVideoDisc(String title, String category) {
        super(title, category);
    }

    public DigitalVideoDisc(String title, String category, String director) {
        super(title, category, director);
    }

    public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
        super(title, category, director, length, cost);
    }

    public boolean search(String title) {
        String arr[] = title.toLowerCase().split(" ");
        List<String> trueTitle = Arrays.asList(this.getTitle().toLowerCase().split(" "));

        for (String token : arr) {
            if (!trueTitle.contains(token)) {
                return false;
            }
        }

        return true;
    }

    public void play() throws PlayerException {
        if (this.getLength() <= 0) {
            System.err.println("ERROR: The length is 0 and can not be played!");
            throw (new PlayerException());
        }
        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLength());
    }

    public int compareTo(Object o) {
        DigitalVideoDisc m = (DigitalVideoDisc) o; //downcast pose a potential issue.
        return ((Float) this.getCost()).compareTo((Float) m.getCost());
    }

}
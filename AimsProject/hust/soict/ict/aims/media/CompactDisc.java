package hust.soict.ict.aims.media;

import java.util.ArrayList;

import hust.soict.ict.aims.media.*;
import hust.soict.ict.aims.PlayerException;

public class CompactDisc extends Disc implements Playable, Comparable {
    private String artist;
    private int length;
    private ArrayList<Track> tracks = new ArrayList<Track>();

    public CompactDisc(String title) {
        super(title);
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void addTrack(Track track) {
        if (!tracks.contains(track)) {
            tracks.add(track);
            System.out.println("Track added!");
        } else
            System.out.println("Already added!");
    }

    public void removeTrack(Track track) {
        if (!tracks.contains(track)) {
            System.out.println("No such track exists!");
        } else {
            tracks.remove(track);
            System.out.println("Track deleted!");
        }
    }

    public int getLength() {
        int len = 0;
        for (Track track : tracks) {
            len += track.getLength();
        }
        return len;
    }

    public void play() throws PlayerException {

        if (this.getLength() <= 0) {
            System.err.println("ERROR: The length is 0 and can not be played!");
            throw (new PlayerException());
        }

        System.out.println("Compact Disc: " + this.getTitle());
        System.out.println("Length of tracks: " + tracks.size());
        for (Track track : tracks) {
            try {
                track.play();
            } catch (PlayerException e) {
                e.printStackTrace();
                throw (new PlayerException());
            }
        }
    }

    public int getNumOfTrack() {
        return tracks.size();
    }

    public int compareTo(Object o) {
        CompactDisc m = (CompactDisc) o; //downcast pose a potential issue.
        if (((Integer) this.getNumOfTrack()).compareTo((Integer) m.getNumOfTrack()) == 0) {
            return ((Integer) this.getLength()).compareTo((Integer) this.getLength());
        }

        return ((Integer) this.getNumOfTrack()).compareTo((Integer) m.getNumOfTrack());
    }
}
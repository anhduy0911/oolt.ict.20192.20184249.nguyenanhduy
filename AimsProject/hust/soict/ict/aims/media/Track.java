package hust.soict.ict.aims.media;

import hust.soict.ict.aims.PlayerException;

public class Track implements Playable, Comparable {
    private String title;
    private int length;

    public Track(int length) {
        this.length = length;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void play() throws PlayerException {

        if (this.getLength() <= 0) {
            System.err.println("ERROR: The length is 0 and can not be played!");
            throw (new PlayerException());
        }

        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLength());
    }

    public int compareTo(Object o) {
        Track m = (Track) o; //downcast pose a potential issue.
        return this.getTitle().compareTo(m.getTitle());
    }

}
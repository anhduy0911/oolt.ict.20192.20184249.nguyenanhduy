package hust.soict.ict.aims.media;

public abstract class Media implements Comparable<Media> {

    private static int idIncrement = 1;

    private int id;
    private String title;
    private String category;
    private float cost;

    public Media() {
        id = idIncrement;
        idIncrement++;
    }

    public Media(String title) {
        this.title = title;
        id = idIncrement;
        idIncrement++;
    }

    public Media(String title, String category) {
        id = idIncrement;
        idIncrement++;
        this.title = title;
        this.category = category;
    }

    public Media(String title, String category, float cost) {
        id = idIncrement;
        idIncrement++;
        this.title = title;
        this.category = category;
        this.cost = cost;
    }

    public int getId() {
        return id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object obj) {
        try {
            if (obj.getClass() == this.getClass()) {
                Media o = (Media) obj;
                if (o.getCost() == this.getCost() && (o.getTitle() == this.getTitle()))
                    return true;
                else
                    return false;
            } else
                return false;
        } catch (NullPointerException e1) {
            System.out.println(e1.getMessage());
        } catch (ClassCastException e2) {
            System.out.println(e2.getMessage());
        } finally {
            return false;
        }
    }

    @Override
    public int compareTo(Media media) {
        try {
            if (media.getClass() == this.getClass()) {
                if ((media.getCost() == this.getCost() && (media.getTitle().equals(this.getTitle()))))
                    return 0;
                else if (this.getTitle().equals(media.getTitle())) {
                    if (this.getCost() < media.getCost())
                        return -1;
                    else
                        return 1;
                } else
                    return this.getTitle().compareTo(media.getTitle());
            } else
                return -1;
        } catch (NullPointerException e) {
            System.out.println(e.getMessage());
        } finally {
            return -1;
        }
    }
}
package hust.soict.ict.aims.media;

import hust.soict.ict.aims.PlayerException;

public interface Playable {
    public void play() throws PlayerException;
}
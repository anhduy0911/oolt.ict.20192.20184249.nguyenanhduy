package hust.soict.ict.aims.order;

import java.util.ArrayList;

import java.time.LocalDate;
import hust.soict.ict.aims.media.Media;


public class Orders {

    public static final int MAXIMUM_ORDER = 10;
    public static final int MAXIMUM_NUM_OF_ORDERS = 10;

    private static int numOfOrders; 
    
    private static int idIncrement = 1;

    private int id;
    private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
    
    //private int quantityOrdered = 0;

    private LocalDate dateOrdered;
    private Media freeItem = null;

    // static factory method to check whether the user has reached maximum of orders before actually create one!
    public static Orders createOrder(LocalDate date)
    {
        if (numOfOrders < MAXIMUM_NUM_OF_ORDERS) {
            Orders orders = new Orders(date);
            return orders;
        }
        System.out.println("Maximum number of orders created!");
        return null;
    }

    private Orders(LocalDate date) {
            id = idIncrement;
            dateOrdered = date;
            idIncrement++;
            numOfOrders ++;
    }
    
    public int getId() {
        return id;
    }

    public LocalDate getDateOrdered() {
        return dateOrdered;
    }
    
    public void setDateOrdered(LocalDate dateOrdered) {
        this.dateOrdered = dateOrdered;
    }

    /*
    public void addDigitalVideoDisc(DigitalVideoDisc[] dvdList)
    {
        for (int i = 0; i < dvdList.length; i++) {
            if (quantityOrdered < MAXIMUM_ORDER) {
                videosOrdered[quantityOrdered] = dvdList[i];
                quantityOrdered ++;
            }
            else{
                System.out.println("DVD " + dvdList[i].getTitle() + " can not add because the order is full!");
            }
        }
    }
    */ 
    // for me the two func are of no difference, but when calling maybe the second way is more convinient ! :D 
    public void addMedia(Media... mediaList)
    {
        for (int i = 0; i < mediaList.length; i++) {
            if (itemsOrdered.size() < MAXIMUM_ORDER) {
                addMedia(mediaList[i]);
            }
            else{
                System.out.println("Item " + ((Media)mediaList[i]).getTitle() + " can not add because the order is full!");
            }
        }
    }


    public void addMedia(Media media) {
        if (itemsOrdered.size() == MAXIMUM_ORDER) {
            System.out.println("Item " + media.getTitle() + " can not add because the order is full!");
            return;
        }
        if (itemsOrdered.size() >= MAXIMUM_ORDER -1) {
            System.out.println("The order list is almost full");
        }
        if(!itemsOrdered.contains(media))
        {
            itemsOrdered.add(media);
            System.out.println("Item added!");
        }
        else
            System.out.println("Already added!");
    }

    public void addMedia(Media m1, Media m2) {
        this.addMedia(m1);
        this.addMedia(m2);
    }


    public void removeMedia(Media media)
    {
        for (Media me : itemsOrdered) {
            if (me.equals(media)) {
                itemsOrdered.remove(me);
                System.out.println("Item removed!");
                return;
            }
        }
        
        System.out.println("There is no such item in the order!");
    }

    public void removeMedia(int id)
    {
        for (Media me : itemsOrdered) {
            if (me.getId() == id) {
                itemsOrdered.remove(me);
                System.out.println("Item removed!");
                return;
            }
        }
        
        System.out.println("There is no such item in the order!");
    }

    public float totalCost()
    {
        float cost = 0; 
        Media free = getALuckyItem();

        for (int i = 0; i < itemsOrdered.size(); i++) {
            if (free != null && !free.equals(itemsOrdered.get(i))) {
                cost += itemsOrdered.get(i).getCost();
            }
        }
        return cost;
    }

    private Media getALuckyItem()
    {
        if(itemsOrdered.size() >1){
            double ind = Math.random();
            freeItem = itemsOrdered.get((int)Math.round(ind * itemsOrdered.size()));
            return freeItem;
        }
        return null;
    }

    public void printOrder()
    {
        System.out.println("Date: " + dateOrdered);
        System.out.println("Order items: ");
        for (int i=0; i < itemsOrdered.size(); i++) {
            if (freeItem != null && freeItem.equals(itemsOrdered.get(i))) {
                System.out.println((i+1) + ".<LUCKY> Media - " + itemsOrdered.get(i).getId()
                                    + " - " + itemsOrdered.get(i).getTitle()
                                    + " - " + itemsOrdered.get(i).getCategory()
                                    + " - " + 0 + " $.");
            }
            else
                System.out.println((i+1) + ". Media - " + itemsOrdered.get(i).getId()
                                    + " - " + itemsOrdered.get(i).getTitle()
                                    + " - " + itemsOrdered.get(i).getCategory()
                                    + " - " + itemsOrdered.get(i).getCost() + " $.");
        }
    }
}
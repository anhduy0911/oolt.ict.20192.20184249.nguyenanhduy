package hust.soict.ict.aims;

import java.time.LocalDate;
import hust.soict.ict.aims.media.*;
import hust.soict.ict.aims.order.Orders;

import java.util.Scanner;

import java.util.List;
import java.util.ArrayList;

public class Aims {

    public static void showMenu() {

        System.out.println("Order Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1. Create new order");
        System.out.println("2. Add item to the order");
        System.out.println("3. Delete item by id");
        System.out.println("4. Display the items list of order");
        System.out.println("0. Exit");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 0-1-2-3-4");
    }

    public static Orders createOrder() {
        LocalDate date = LocalDate.now();

        Orders order = Orders.createOrder(date);
        if (order != null)
            System.out.println("Order created!");
        return order;
    }

    private static void addDVD(Orders order, Scanner scanner) {
        DigitalVideoDisc dc1 = new DigitalVideoDisc("");
        System.out.println("Input title:");
        dc1.setTitle(scanner.nextLine());
        System.out.println("Input category:");
        dc1.setCategory(scanner.nextLine());
        System.out.println("Input director:");
        dc1.setDirector(scanner.nextLine());
        System.out.println("Input length:");
        dc1.setLength(scanner.nextInt());
        System.out.println("Input cost:");
        dc1.setCost(scanner.nextFloat());

        System.out.println("Do you want to play? 1-Y 2-N");
        int n = scanner.nextInt();
        if (n == 1) {
            try {
                dc1.play();
            } catch (PlayerException e) {
                e.getMessage();
            }
        }

        order.addMedia(dc1);
    }

    private static void addCD(Orders order, Scanner scanner) {
        CompactDisc disc = new CompactDisc("");

        System.out.print("Enter the title: ");
        disc.setTitle(scanner.nextLine());
        System.out.print("Enter the category: ");
        disc.setCategory(scanner.nextLine());

        System.out.println("Enter number of tracks to add:");
        int n = scanner.nextInt();
        scanner.nextLine();
        for (int i = 0; i < n; i++) {
            Track tmp = new Track(0);
            System.out.println("Enter track's title:");
            tmp.setTitle(scanner.nextLine());
            System.out.println("Enter track's length:");
            tmp.setTitle(scanner.nextLine());
            disc.addTrack(tmp);
        }
        System.out.print("Enter the cost :");
        disc.setCost(scanner.nextFloat());
        System.out.println("Do you want to play? 1-Y 2-N");
        n = scanner.nextInt();
        if (n == 1) {
            try {
                disc.play();
            } catch (PlayerException e) {
                e.printStackTrace();
            }
        }
        order.addMedia(disc);
    }

    private static void addBook(Orders order, Scanner scanner) {
        System.out.print("Enter the title: ");
        String name = scanner.nextLine();
        System.out.print("Enter the category: ");
        String cat = scanner.nextLine();

        System.out.println("Enter number of authors to add");
        int n = scanner.nextInt();
        List<String> a = new ArrayList<String>();
        for (int i = 0; i < n; i++) {
            System.out.println("Enter author's name:");
            String tmp = scanner.nextLine();
            a.add(tmp);
        }

        System.out.print("Enter the cost :");
        float cost = scanner.nextFloat();
        order.addMedia(new Book(name, cat, cost, a));
    }

    public static void addItem(Orders order, Scanner scanner) {
        if (order == null) {
            System.out.println("Order has not been created!");
            return;
        }
        System.out.println("Input an item to add: 1-Digital Video Disc 2-Book 3-Compact Disc");
        int choice = scanner.nextInt();
        scanner.nextLine();
        switch (choice) {
            case 1:
                addDVD(order, scanner);
                break;
            case 2:
                addBook(order, scanner);
                break;
            case 3:
                addCD(order, scanner);
                break;
            default:
                System.out.println("Invalid operation!");
                break;
        }
    }

    public static void deleteItem(Orders order, Scanner scanner) {
        System.out.println("Enter ID of item to delete:");
        int id = scanner.nextInt();
        order.removeMedia(id);
    }

    public static void displayOrder(Orders order) {
        order.printOrder();
    }

    public static void main(String[] args) {

        Thread daemonThread = new Thread(new MemoryDaemon());
        daemonThread.setDaemon(true);
        daemonThread.start();

        Scanner scanner = new Scanner(System.in);
        boolean isRunning = true;
        Orders order = null;
        while (isRunning) {
            showMenu();
            int opt = scanner.nextInt();
            switch (opt) {
                case 1:
                    order = createOrder();
                    break;
                case 2:
                    addItem(order, scanner);
                    break;
                case 3:
                    deleteItem(order, scanner);
                    break;
                case 4:
                    displayOrder(order);
                    break;
                case 0:
                    isRunning = false;
                    break;
                default:
                    System.out.println("Invalid option!");
                    break;
            }
        }
        scanner.close();
        // test compareTo()

        ArrayList<DigitalVideoDisc> collection = new ArrayList<DigitalVideoDisc>();
        DigitalVideoDisc dvd1 = new DigitalVideoDisc("Lion King 3", "", "", 0, 2.1f);
        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Lion King 1", "", "", 0, 5.6f);
        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Lion King 2", "", "", 0, 1.2f);

        collection.add(dvd1);
        collection.add(dvd2);
        collection.add(dvd3);

        System.out.println("Befor sort:");
        java.util.Iterator iter = collection.iterator();

        while (iter.hasNext()) {
            System.out.println(((DigitalVideoDisc) iter.next()).getTitle());
        }
        java.util.Collections.sort(collection);

        System.out.println("Sorted:");
        iter = collection.iterator();
        while (iter.hasNext()) {
            System.out.println(((DigitalVideoDisc) iter.next()).getTitle());
        }

        ArrayList<CompactDisc> collection2 = new ArrayList<CompactDisc>();
        CompactDisc cd1 = new CompactDisc("Gone girl 1");
        cd1.addTrack(new Track(3));
        cd1.addTrack(new Track(4));
        cd1.addTrack(new Track(5));
        CompactDisc cd2 = new CompactDisc("Gone girl 2");
        cd2.addTrack(new Track(4));
        cd2.addTrack(new Track(5));

        CompactDisc cd3 = new CompactDisc("Gone girl 3");
        cd3.addTrack(new Track(5));

        collection2.add(cd1);
        collection2.add(cd2);
        collection2.add(cd3);

        System.out.println("Befor sort:");
        java.util.Iterator iter2 = collection2.iterator();

        while (iter2.hasNext()) {
            System.out.println(((CompactDisc) iter2.next()).getTitle());
        }
        java.util.Collections.sort(collection2);

        System.out.println("Sorted:");
        iter2 = collection2.iterator();
        while (iter2.hasNext()) {
            System.out.println(((CompactDisc) iter2.next()).getTitle());
        }

    }

}
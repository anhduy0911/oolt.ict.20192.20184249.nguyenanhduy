package hust.soict.ict.aims;

import java.time.LocalDate;
import hust.soict.ict.aims.disc.DigitalVideoDisc;
import hust.soict.ict.aims.order.Orders;

public class DiskTest {

    public static void main(String[] args) {
        
        DigitalVideoDisc jungleDVD = new DigitalVideoDisc("Jungle Book");
        System.out.println("Find title: " + jungleDVD.search("jungle"));
        
        DigitalVideoDisc cindrellaDVD = new DigitalVideoDisc("Cindrella");
        // This is of lab 3
        swap(jungleDVD,cindrellaDVD);
        System.out.println("jungle dvd title: " + jungleDVD.getTitle());
        System.out.println("cindrella dvd title: " + cindrellaDVD.getTitle());

        changeTitle(jungleDVD, cindrellaDVD.getTitle());
        System.out.println("jungle dvd title: " + jungleDVD.getTitle());
        
        //This part is in lab 5
        LocalDate date = LocalDate.now();
        Orders order = Orders.createOrder(date);
        
        order.addDigitalVideoDisc(new DigitalVideoDisc("Lion King","Animation","Mr.A",100,19.5f)); 
        order.addDigitalVideoDisc(new DigitalVideoDisc("Lion King 2","Animation","Mr.A",100,20.4f)); 
        order.addDigitalVideoDisc(new DigitalVideoDisc("Lion King 3","Animation","Mr.A",100,10.5f)); 

        System.out.println("Total cost: " + order.totalCost());
        order.printOrder();



    }

    public static void swap(Object ob1, Object ob2)
    {
        Object tmp = ob1;
        ob1 = ob2;
        ob2 = tmp;
    }

    public static void changeTitle(DigitalVideoDisc disc, String title)
    {
        String oldTitle = disc.getTitle();
        disc.setTitle(title);
        disc = new DigitalVideoDisc(oldTitle);
    }

}
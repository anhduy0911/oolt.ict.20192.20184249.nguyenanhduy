package hust.soict.gui.awt;

import java.awt.*;
import java.awt.event.*;

public class AWTCounter extends Frame implements ActionListener {

	private Label countLabel;
	private TextField countTxt;
	private Button countBtn;
	private int count = 0;

	public AWTCounter() {
		setLayout(new FlowLayout());
		countLabel = new Label("Counter");
		add(countLabel);

		countTxt = new TextField(count + "", 10);
		countTxt.setEditable(false);
		add(countTxt);

		countBtn = new Button("Count");
		add(countBtn);

		countBtn.addActionListener(this);

		setTitle("AWT Counter");
		setSize(250, 100);
		
		setVisible(true);
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.out.println("Closing...");
				System.exit(0);
			}
		});
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		++count;
		System.out.println("Counting..");
		countTxt.setText(count + "");
	}
	
	public static void main(String[] args) {
		AWTCounter app = new AWTCounter();

	}
}

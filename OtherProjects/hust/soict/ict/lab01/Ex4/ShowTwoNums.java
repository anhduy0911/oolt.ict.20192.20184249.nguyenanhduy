package hust.soict.ict.lab01.Ex4;
import javax.swing.JOptionPane;

public class ShowTwoNums
{
    public static void main(String[] args)
    {
        String num1,num2;
        String result = "The 2 inputed numbers are: ";
        num1 = JOptionPane.showInputDialog(null, "Enter 1st number: ", "First number Input", JOptionPane.INFORMATION_MESSAGE);
        num2 = JOptionPane.showInputDialog(null, "Enter 2nd number: ", "Second number Input", JOptionPane.INFORMATION_MESSAGE);
        JOptionPane.showMessageDialog(null, result + num1 + " and " + num2, "Show 2 numbers", JOptionPane.INFORMATION_MESSAGE);
        System.exit(0);
    }
}
package hust.soict.ict.lab01.Ex5;
import java.util.Scanner;

public class BasicOperation
{
    public static void arithmeticOperation(double a,double b)
    {
        System.out.println("Sum: " + (a+b));
        System.out.println("Difference: " + (a-b));
        System.out.println("Product: " + (a*b));
        if (b != 0)
            System.out.println("Quotient: " + (a/b));
        else
            System.out.println("The devisor must be other than 0!!!");
    }
    public static void main(String[] args)
    {
        System.out.println("Input 2 number: ");
        Scanner scanner = new Scanner(System.in);
        double a = scanner.nextDouble();
        double b = scanner.nextDouble();
        arithmeticOperation(a, b);
        scanner.close();
        System.exit(0);
    }
}
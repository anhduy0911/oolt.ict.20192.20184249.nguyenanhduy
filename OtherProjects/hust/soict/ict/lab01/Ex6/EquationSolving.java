package hust.soict.ict.lab01.Ex6;

import java.util.Scanner;
import java.lang.Math;

public class EquationSolving
{
    public static void firstDegreeEquation(double a,double b,double c)
    {
        if (a != 0)
        {
            double res = (c-b)/a;
            System.out.println("The result: " + res);
        }
        else{
            if (b == c)
                System.out.println("The eqt has infinite solutions");
            else
                System.out.println("The eqt has no solution");
        }
    }

    public static void firstDegreeEquation(double a1,double b1,double c1,double a2,double b2,double c2)
    {   
        if(a1*b2-a2*b1 != 0 && a1 != 0)
        {
            double y = (a1*c2 - a2*c1)/(a1*b2-a2*b1);
            double x = (c1 - b1*y)/a1;
            System.out.println("The result: " + x + " and " + y);
        }
        else{
            if (c1 == c2)
                System.out.println("The eqt has infinite solutions");
            else
                System.out.println("The eqt has no solution");
        }
    }

    public static void secondDegreeEquation(double a,double b,double c)
    {
        if(a!=0)
        {
            double delta = Math.pow(b,2) - 4*a*c;
            if (delta >= 0)
            {
                double x1 = (-b - Math.sqrt(delta))/(2*a);
                double x2 = (-b + Math.sqrt(delta))/(2*a);
                System.out.println("The result: " + x1 + " and " + x2);
            }
            else
                System.out.println("The eqt has no solution");
        }
        else
            firstDegreeEquation(b, c, 0);
    }
    public static void main(String[] args)
    {
        System.out.println("Choose operation 1 2 3 to perform: ");
        System.out.println("1. First order equation with 1 variable.");
        System.out.println("2. First order equation with 2 variables.");
        System.out.println("3. Second order equation with 1 variable.");
        
        Scanner scanner = new Scanner(System.in);
        int opt = scanner.nextInt();
        switch (opt) {
            case 1:
                System.out.println("Input 3 coefficients: ");
                double a = scanner.nextDouble();
                double b = scanner.nextDouble();
                double c = scanner.nextDouble();
                firstDegreeEquation(a, b, c);
                break;
            case 2:
                System.out.println("Input 3 coefficients of 1st eqt: ");
                double a1 = scanner.nextDouble();
                double b1 = scanner.nextDouble();
                double c1 = scanner.nextDouble();
                System.out.println("Input 3 coefficients of 2nd eqt: ");
                double a2 = scanner.nextDouble();
                double b2 = scanner.nextDouble();
                double c2 = scanner.nextDouble();
                firstDegreeEquation(a1, b1, c1, a2, b2, c2);
                break;
            case 3:
                System.out.println("Input 3 coefficients: ");
                double a3 = scanner.nextDouble();
                double b3 = scanner.nextDouble();
                double c3 = scanner.nextDouble();
                secondDegreeEquation(a3, b3, c3);
                break;
            default:
                System.out.println("Invalid operation");
                break;
        }

        scanner.close();
        System.exit(0);
    }
}
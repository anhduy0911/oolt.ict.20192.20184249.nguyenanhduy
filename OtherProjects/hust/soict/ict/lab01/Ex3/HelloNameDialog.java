package hust.soict.ict.lab01.Ex3;
import javax.swing.JOptionPane;

public class HelloNameDialog
{
    public static void main(String[] args)
    {
        String name;
        name = JOptionPane.showInputDialog(null,"Introduce yourself: ");
        JOptionPane.showMessageDialog(null, "Welcome " + name + "!!!");
        System.exit(0);
    }
}
package hust.soict.ict.garbage;

import java.util.Random;

public class ConcatInLoops {
        public static void main(String[] args) {
            Random rand = new Random(123);
            long start = System.currentTimeMillis();
            String s = "";
            for (int i = 0; i < 65536; i++) {
                s += rand.nextInt(2);
            }
            System.out.println(System.currentTimeMillis() - start);

            start = System.currentTimeMillis();
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < 65536; i++) {
                builder.append(rand.nextInt(2));
            }
            s = builder.toString();
            System.out.println(System.currentTimeMillis() - start);
            
    }
}
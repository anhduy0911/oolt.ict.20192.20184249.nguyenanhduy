package hust.soict.ict.garbage;

import java.util.Scanner;
import java.io.File;

public class NoGarbage
{
    public static void main(String[] args) {
        try {
            String path = System.getProperty("user.dir") + "\\text.txt";
            File file = new File(path);
            Scanner scanner = new Scanner(file);
    
            long start = System.currentTimeMillis();
    
            StringBuilder s = new StringBuilder();
    
            while (scanner.hasNextLine())
            {
                s.append(scanner.nextLine());
            }
            String res = s.toString();
            System.out.println("The total time: " + (System.currentTimeMillis() - start));
    
            scanner.close();

        }catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
}
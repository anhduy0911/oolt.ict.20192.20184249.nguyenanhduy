package hust.soict.ict.garbage;

import java.util.Scanner;
import java.io.File;

public class GarbageCreator
{
    public static void main(String[] args) {

        String path = System.getProperty("user.dir") + "\\text.txt";
        System.out.println(path);
        //FileReader file = new FileReader(path);
        try{
            File file = new File(path);
            Scanner sc = new Scanner(file);
            long start = System.currentTimeMillis();
            String s = "";
            while (sc.hasNextLine())
            {
                s += sc.nextLine();
            }
            System.out.println("The total time: " + (System.currentTimeMillis() - start));
    
            sc.close();

        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }

    }
}
package hust.soict.ict.exception;

import java.util.Scanner;

public class StudentTest {

    public static String getFormatedBd(String bd) throws IllegalBirthdayException {
        String[] toks = bd.split("[/]");

        if (toks.length != 3) {
            throw new IllegalBirthdayException("Wrong format!");
        }
        int d = Integer.parseInt(toks[0]);
        int m = Integer.parseInt(toks[1]);
        int y = Integer.parseInt(toks[2]);

        if (d < 0 || d > 31) {
            throw new IllegalBirthdayException("Day must be between 1 and 31");
        }

        if (m < 0 || m > 12) {
            throw new IllegalBirthdayException("Month must be between 1 and 12");
        }

        return bd;
    }

    public static float getInGpa(float gpa) throws IllegalGPAException {
        if (gpa < 0 || gpa > 4) {
            throw new IllegalGPAException();
        }

        return gpa;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Student s = new Student();

        System.out.println("Enter student's ID:");
        s.setId(scanner.nextLine());
        System.out.println("Enter student's name:");
        s.setName(scanner.nextLine());
        System.out.println("Enter student's birthday:");
        try {
            s.setBirthday(getFormatedBd(scanner.nextLine()));

        } catch (IllegalBirthdayException e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }

        System.out.println("Enter student's GPA:");
        try {
            s.setGpa(getInGpa(scanner.nextFloat()));

        } catch (IllegalGPAException e) {
            System.err.println("ERROR: GPA must be >= 0 and <= 4");
        }

        scanner.close();
    }
}
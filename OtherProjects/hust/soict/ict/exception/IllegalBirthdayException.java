package hust.soict.ict.exception;

public class IllegalBirthdayException extends Exception {
    public IllegalBirthdayException(String message) {
        super(message);
    }
}
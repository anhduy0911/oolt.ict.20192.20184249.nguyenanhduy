package hust.soict.ict.lab02.Ex4;

import java.util.Scanner;

public class Ex4 {

    public static void printStar(int n)
    {
        int totalStars = 2*n -1;
        for (int i = 0; i < n; i++) {
            int stars = 2*i +1;
            for (int j = 0; j < (totalStars-stars)/2; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j < stars; j++) {
                System.out.print("*");
            }
            for (int j = 0; j < (totalStars-stars)/2; j++) {
                System.out.print(" ");
            }
            System.out.println("");
        }
    }
    public static void main(String[] args) {
        System.out.println("Input lines of star: ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        printStar(n);

        scanner.close();
        System.exit(0);
    }
    
}
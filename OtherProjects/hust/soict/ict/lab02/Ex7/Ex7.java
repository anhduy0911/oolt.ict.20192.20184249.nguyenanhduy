package hust.soict.ict.lab02.Ex7;

import java.util.Scanner;

public class Ex7 {

    private static int[][] readData(Scanner scanner, int sizex,int sizey)
    {
        int[][] res = new int[sizex][sizey];
        for (int i = 0; i < sizex; i++) {
            for (int j = 0; j < sizey; j++) {
                res[i][j] = scanner.nextInt();
            }
        }
        return res;
    }

    private static void sumMatrix(int[][] a,int[][] b)
    {
        System.out.println("The sum matrix: ");
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                System.out.print((a[i][j] + b[i][j]) + " ");
            }
            System.out.println("");
        }
    }
    public static void main(String[] args) {
        System.out.println("Enter the size of the matrix:");
        Scanner scanner = new Scanner(System.in);
        int sizex = scanner.nextInt();
        int sizey = scanner.nextInt();
        System.out.println("Input the 1st matrix:");
        int[][] a = readData(scanner,sizex, sizey);
        System.out.println("Input the 2nd matrix:");
        int[][] b = readData(scanner,sizex, sizey);
        sumMatrix(a,b);

        scanner.close();
    }
}
package hust.soict.ict.lab02.Ex5;

import java.util.*;

public class Ex5 {

    private static HashMap<Integer,Integer> daysOfMonths = new HashMap<Integer,Integer>();
    
    private static void seedingData()
    {
        daysOfMonths.put(1,31);
        daysOfMonths.put(2,28);
        daysOfMonths.put(3,31);
        daysOfMonths.put(4,30);
        daysOfMonths.put(5,31);
        daysOfMonths.put(6,30);
        daysOfMonths.put(7,31);
        daysOfMonths.put(8,31);
        daysOfMonths.put(9,30);
        daysOfMonths.put(10,31);
        daysOfMonths.put(11,30);
        daysOfMonths.put(12,31);
    }
    public static void main(String[] args) {
        seedingData();
        System.out.println("Input the month in number:");
        Scanner scanner = new Scanner(System.in);
        int month = scanner.nextInt();
        while (daysOfMonths.get(month) == null)
        {
            System.out.println("Please input a valid month");
            month = scanner.nextInt();
        }
        System.out.println("The month has " + daysOfMonths.get(month) + " days.");
        scanner.close();
    }
    
}
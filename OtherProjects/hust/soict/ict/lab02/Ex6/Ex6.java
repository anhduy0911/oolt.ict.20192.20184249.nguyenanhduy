package hust.soict.ict.lab02.Ex6;

import java.util.Scanner;
import java.util.Arrays;

public class Ex6 {

    public static void main(String[] args) {
        // array input by user
        System.out.println("Enter the size of the array:");
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int[] arr = new int[size];
        int sum = 0;
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
            sum += arr[i];
        }
        Arrays.sort(arr);
        System.out.print("The sorted array: ");
        for (int i : arr) {
            System.out.print(i + " ");
        }
        System.out.println("\nThe sum: " + sum);
        System.out.println("The average: " + (sum/size));

        scanner.close();
    }
}
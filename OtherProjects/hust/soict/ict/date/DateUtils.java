package hust.soict.ict.date;

import java.time.LocalDate;
import java.util.Arrays;

public class DateUtils {

    public static void compareDate(MyDate d1, MyDate d2)
    {
        LocalDate date1 = LocalDate.of(d1.getYear(), d1.getMonth(), d1.getDay());
        LocalDate date2 = LocalDate.of(d2.getYear(), d2.getMonth(), d2.getDay());
        if (date1.compareTo(date2) > 0) {
            System.out.println("Date 1 is greater.");
        }
        else if (date1.compareTo(date2) < 0)
        {
            System.out.println("Date 2 is greater.");
        }
        else
        {
            System.out.println("The 2 dates are equal.");
        }
    }

    public static void sortDate(MyDate... dates)
    {
        LocalDate[] ds = new LocalDate[dates.length];

        for (int i = 0; i < ds.length; i++) {
            ds[i] = LocalDate.of(dates[i].getYear(), dates[i].getMonth(), dates[i].getDay());
        }

        Arrays.sort(ds);

        System.out.println("Sorted dates: ");

        for (LocalDate localDate : ds) {
            System.out.println(localDate);
        }
    }

}
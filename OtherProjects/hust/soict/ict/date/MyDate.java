package hust.soict.ict.date;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Scanner;

public class MyDate {
    
    private int day;
    private int month;
    private int year;

    public MyDate() {
        super();
        LocalDate date = LocalDate.now();
        this.day = date.getDayOfMonth();
        this.month = date.getMonthValue();
        this.year = date.getYear();
    }

    //assumming all the number is right in value
    public MyDate(int day,int month,int year) {
        super();
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public MyDate(String string)
	{
        // February 18th 2020
		//Consider using space as delimiter
		String arrOfString[] = string.split(" ", -1);
		switch(arrOfString[0])
		{
		case "January" : month = 1; break;
		case "February" : month = 2; break;
		case "March" : month = 3; break;
		case "April" : month = 4; break;
		case "May" : month = 5; break;
		case "June" : month = 6; break;
		case "July" : month = 7; break;
		case "August" : month = 8; break;
		case "September" : month = 9; break;
		case "October" : month = 10; break;
		case "November" : month = 11; break;
		case "December" : month = 12; break;
		default : System.out.println("Invalid input!");
		
		}
		setYear(Integer.parseInt(arrOfString[2]));
        setDay(Integer.parseInt(arrOfString[1].substring(0, arrOfString[1].length() - 2)));
        
	}

    //getter and setter
	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
        if(month >=1 && month <13)
            this.month = month;
        else
            System.out.println("Invalid month");
    }

    // I only create overload for setMonth with string, since there are plenty of cases if create overload method for setYear or setDay :D
    // If there is any way can you please give me a hint? Thank you. 
    public void setMonth(String month)
    {
        switch (month) {
            case "January":
                this.month = 1;
                break;
            case "Febuary":
                this.month = 2;
                break;
            case "March":
                this.month = 3;
                break;
            case "April":
                this.month = 4;
                break;
            case "May":
                this.month = 5;
                break;
            case "June":
                this.month = 6;
                break;
            case "July":
                this.month = 7;
                break;
            case "August":
                this.month = 8;
                break;
            case "September":
                this.month = 9;
                break;
            case "October":
                this.month = 10;
                break;
            case "November":
                this.month = 11;
                break;
            case "December":
                this.month = 12;
                break;
            default:
                System.out.println("Invalid input! Month would be set default 1");
                this.month = 1;
                break;
        }
    }
    
    public int getDay() {
		return day;
	}

	public void setDay(int day) {
        switch (this.month) {
            case 1: case 3: case 5: case 7: case 8: case 10: case 12:
                if(day >= 1 && day <= 31)
                    this.day = day;
            break;
            case 4: case 6: case 9: case 11:
                if(day >= 1 && day <= 31)
                    this.day = day;
            break;
            case 2:
                if(day >= 1 && day <= 28)
                this.day = day;
            default:
                System.out.println("Invalid day!");
                break;
        }
    }


    
	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
    
    // I use library for these print functions :)
    public void printDate(String format)
	{
        LocalDate date = LocalDate.of(this.year, this.month, this.day);
        System.out.println(date.format(DateTimeFormatter.ofPattern(format)));
        
    }
    
    public void printDate()
    {
        LocalDate date = LocalDate.of(this.year, this.month, this.day);
        System.out.println(date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)));
    }

    public void accept()
	{
        System.out.println("Enter a day in the right form (ect. February 18th 2020): ");
        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine();
        MyDate date = new MyDate(string);
        
        this.day = date.getDay();
        this.month = date.getMonth();
        this.year = date.getYear();

        scanner.close();
	}
}
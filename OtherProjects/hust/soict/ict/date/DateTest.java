package hust.soict.ict.date;

public class DateTest {

	public static void main(String[] args) 
	{
		//test method!!!
		MyDate dateA = new MyDate();
		MyDate dateB = new MyDate(12, 5, 2000);
		MyDate dateC = new MyDate("March 13th 2020");
		
		dateA.printDate();
		dateB.printDate();
        dateC.printDate();
        
        dateB.setMonth("January");
        dateB.printDate("dd/MM/YYYY");
		
        DateUtils.compareDate(dateA,dateB);
        DateUtils.sortDate(dateA,dateB,dateC);
		
	}

}